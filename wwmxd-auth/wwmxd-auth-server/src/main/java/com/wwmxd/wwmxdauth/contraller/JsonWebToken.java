package com.wwmxd.wwmxdauth.contraller;


import com.alibaba.fastjson.JSONObject;
import com.wwmxd.common.msg.ObjectRestResponse;
import com.wwmxd.common.msg.auth.TokenErrorResponse;
import com.wwmxd.common.utils.vo.UserInfo;
import com.wwmxd.wwmxdauth.bean.Audience;
import com.wwmxd.wwmxdauth.feign.IUserService;
import com.wwmxd.wwmxdauth.utils.Des;
import com.wwmxd.wwmxdauth.utils.JwtHelper;
import com.wwmxd.wwmxdauth.utils.ResultMsg;
import com.wwmxd.wwmxdauth.utils.ResultStatusCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * 登录验证
 *
 */
@RestController
@EnableConfigurationProperties(Audience.class)
public class JsonWebToken {


    @Autowired
    private Audience audienceEntity;
    @Autowired
    private IUserService iUserService;

    @RequestMapping(value = "oauth/token",method = RequestMethod.POST)
    public Object getAccessToken(@RequestBody UserInfo userInfo)
    {

        ResultMsg resultMsg = null;
        try
        {
            String password = Des.strDec(userInfo.getPassword(),"xmob","X","MOB");
            UserInfo user=iUserService.validate(userInfo.getUsername(),password);
            if(user.getName()!=null){
                //拼装accessToken
                String accessToken = JwtHelper.createJWT(user.getName(), user.getId()
                        , audienceEntity.getClientId(), audienceEntity.getName(),
                        audienceEntity.getExpiresSecond() * 1000, audienceEntity.getBase64Secret());
                user.setToken(accessToken);
                ObjectRestResponse response=new ObjectRestResponse<UserInfo>();
                return response.data(user);


            }else {
               return new TokenErrorResponse("用户名或密码错误");
            }





        }
        catch(Exception ex)
        {
            ex.printStackTrace();
            resultMsg = new ResultMsg(ResultStatusCode.SYSTEM_ERR.getErrcode(),
                    ResultStatusCode.SYSTEM_ERR.getErrmsg(), null);
            return resultMsg;
        }
    }
    @RequestMapping(value = "oauth/refresh",method = RequestMethod.POST)
    public Object RefreshAccessToken(@RequestBody JSONObject jsoNobject)
    {

        ResultMsg resultMsg = null;
        try
        {
            String token=jsoNobject.getString("token");
            JwtHelper jwt=new JwtHelper();
            UserInfo user=jwt.getUserInfo(token, audienceEntity.getBase64Secret());
            if(user.getName()!=null){
                //拼装accessToken
                String accessToken = JwtHelper.createJWT(user.getName(), user.getId()
                        , audienceEntity.getClientId(), audienceEntity.getName(),
                        audienceEntity.getExpiresSecond() * 1000, audienceEntity.getBase64Secret());
                ObjectRestResponse response=new ObjectRestResponse<UserInfo>();
                return response.data(accessToken);


            }else {
                return new TokenErrorResponse("token刷新失败");
            }        }
        catch(Exception ex)
        {
            ex.printStackTrace();
            resultMsg = new ResultMsg(ResultStatusCode.SYSTEM_ERR.getErrcode(),
                    ResultStatusCode.SYSTEM_ERR.getErrmsg(), null);
            return resultMsg;
        }
    }
}
